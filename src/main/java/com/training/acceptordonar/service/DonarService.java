/**
 * creating Service Interface 
 * to write the UnImplemented methods in main class(ServiceImp)
 */
package com.training.acceptordonar.service;
/**
 * @author akhilesh-a
 *Use Case Name:Blood Bank Management system
 *Squad Number:9
 */

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.training.acceptordonar.model.Donar;

//Enabling feign 
@FeignClient(value="donarApp",url="http://localhost:5555/doner")
public interface DonarService {
	
	@PostMapping("/create")
	public Donar createBloodManagement(@RequestBody Donar donar);
	
	@PutMapping("/updateById")
	public Donar updateBloodManagement(@RequestBody Donar donar);
	
	@DeleteMapping("/deleteById")
	public boolean deleteBloodManagement(@RequestParam("id") int id);
	
	@GetMapping("/getById")
	public Donar getBloodManagement(@RequestParam("id") int id);
	
	@GetMapping("")
	public List<Donar> getBloodManagements();
	
	@GetMapping("/all")
	public List<Donar> getDonars(@RequestParam("id") int acceptorId);
	
	
	@DeleteMapping("/delete")
	public boolean deleteAllByAcceptorId(@RequestParam("id") int id);
	
	@DeleteMapping("")
	public String deleteByAcceptorId(@RequestParam("id") int id);
	

}
