package com.training.acceptordonar.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.acceptordonar.model.Acceptor;
import com.training.acceptordonar.model.Donar;

//defining the business logic  
@Service
public class AcceptorDonarServiceImpl implements AcceptorDonarService {

	// autowire the AcceptorService class
	@Autowired
	private AcceptorService acceptorService;
	
	// autowire the DonarService class
	@Autowired
	private DonarService donarService;

	// getting all acceptor record by using the method findaAll() of JpaRepository
	@Override
	public List<Acceptor> getDetails() {
		List<Acceptor> acceptors = acceptorService.getAcceptors();
		acceptors.forEach(a -> a.setDonarList(donarService.getDonars(a.getId())));
		return acceptors;
	}

	// saving a specific record by using the method save() of JpaRepository
	@Override
	public Acceptor createAcceptorsDonars(Acceptor acceptor) {
		Acceptor acceptor2 = acceptorService.createAcceptor(acceptor);	
		acceptor.getDonarList().forEach(d -> {
			d.setAcceptorId(acceptor2.getId());
			donarService.createBloodManagement(d);
		});
//		Acceptor acceptor3 = getAllById(acceptor2.getId());
		return acceptor;
	}

	// getting a specific record by using the method findById() of JpaRepository
	@Override
	public Acceptor getAllById(int id) {
		Acceptor acceptor = acceptorService.getAcceptor(id);
		List<Donar> donar = donarService.getDonars(acceptor.getId());
		acceptor.setDonarList(donar);
		return acceptor;
	}

	// deleting a specific record by using the method deleteById() of JpaRepository
	@Override
	public String deleteAllById(int id) {
		Acceptor acceptor = acceptorService.getAcceptor(id);
		if (acceptor != null ) {
			String b=donarService.deleteByAcceptorId(acceptor.getId());
			String a=acceptorService.deleteAcceptor(id);
			return a;
		}
		return "id is not present";
	}

	// updating a record
	@Override
	public Acceptor updateAcceptorDonars(Acceptor acceptor) {
		
		Acceptor acceptor2 = acceptorService.updateAcceptor(acceptor);
		acceptor.getDonarList().forEach(d->{
			donarService.updateBloodManagement(d);
		});
//		Acceptor acceptor3 = getAllById(acceptor2.getId());
		return acceptor;
	}

}
