package com.training.acceptordonar;
/**
* Use Case Name:DFTE_FT_M14135_BLOODBANKSYSTEM
* Developer Name:Supritha k
* Squad Number:9
*/

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableEurekaClient
@SpringBootApplication
//this is the feign client 
@EnableFeignClients
public class AcceptordonarApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcceptordonarApplication.class, args);
	}

}
